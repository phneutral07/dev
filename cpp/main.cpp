#include <iostream>
using namespace std;
int calc(int x, int y, char op) {
    switch (op) {
    case '+': return x + y;
    case '-': return x - y;
    case '*': return x * y;
    case '/': return x / y;
    default: throw std::invalid_argument(std::string("unknown operation ") + op);
    }
}
int main()
{
    calc(12,13,+);
	return 0;
}
